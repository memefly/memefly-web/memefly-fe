// React Stuff
import React from "react";
import { Route, Switch,  BrowserRouter as Router } from "react-router-dom";

// Styling
import "./App.scss";

// Components
import Login from "./components/LoginPage/Login";
import Register from "./components/RegisterPage/Register";
import Header from "./components/Navigation/Header";
import GenerateMemePage from "./components/ImgEditor/GenerateMemePage";
import {LearnMore} from "./components/learnMore/LearnMore";
import Page404 from "./components/Page404/Page404"


import Userprofile from "./components/UserProfile/Userprofile";

function App() {

  return (
    <Router>
      <Header/>

      <Switch>
        <Route exact path="/" component={GenerateMemePage} />
        <Route path="/Login" component={Login} />
        <Route path="/Register" component={Register} />
        <Route path="/LearnMore" component={LearnMore} />
        <Route path="/Profile" component={Userprofile} />
        <Route component={Page404} />
      </Switch>
    </Router>
  );
}

export default App;
