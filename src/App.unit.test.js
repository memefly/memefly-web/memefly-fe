import { AlertTitle } from "@material-ui/lab";
import { shallow, mount } from "enzyme";
import React from "react";
import { Route, MemoryRouter } from "react-router-dom";
import App from "./App";
import GenerateMemePage from "./components/ImgEditor/GenerateMemePage";
import { LearnMore } from "./components/learnMore/LearnMore";
import Login from "./components/LoginPage/Login";
import Page404 from "./components/Page404/Page404";
import Register from "./components/RegisterPage/Register";

const shallowApp = () => {
	return shallow(<App />);
};

it("renders without crashing", () => {
	shallowApp();
	const generateButton = <button>GENERATE MEME</button>;
	const warningsign = <AlertTitle>Warning</AlertTitle>;
});

let pathMap = {};
describe("routes using array of routers", () => {
	beforeAll(() => {
		const component = shallowApp();
		console.log("component: ", component);

		pathMap = component.find(Route).reduce((pathMap, route) => {
			const routeProps = route.props();
			pathMap[routeProps.path] = routeProps.component;
			return pathMap;
		}, {});
		console.log(pathMap);
	});

	it("should show GenerateMemePage component for / router (getting array of routes)", () => {
		expect(pathMap["/"]).toBe(GenerateMemePage);
	});
	it("should show LearnMore component for /LearnMore router", () => {
		expect(pathMap["/LearnMore"]).toBe(LearnMore);
	});
	it("should show Register component for /Register router", () => {
		expect(pathMap["/Register"]).toBe(Register);
	});
	it("should show Login component for /Login router", () => {
		expect(pathMap["/Login"]).toBe(Login);
	});
	it("should show No matching component for a route not defined", () => {
		expect(pathMap["undefined"]).toBe(Page404);
	});
});
