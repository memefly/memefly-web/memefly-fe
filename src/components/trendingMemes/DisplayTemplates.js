import Axios from 'axios';
import React, { useEffect, useState } from 'react';

function DisplayTemplates() {

    const [templates, setTemplates] = useState({});
    const [template, setTemplate] = useState(null);


    function axiosConfig(query) {
					return {
						url: "http://memefly.herokuapp.com/api/memes/base",
						method: "POST",
						data: {
							query
						}
					};
				}

  function getBaseMeme(id, rand = false) {
			return `
        query{
            getBaseMeme(id:${id}, rand:${rand}){
                message
                fetched
                meme_bounding_box
                meme_id
                meme_url
                meme_text
            }
        }
    `;
		}

  useEffect(() => {
    Axios(axiosConfig(getBaseMeme(null, true)))
    .then(res => {
      // console.log(res);
      setTemplates(res.data.data.getBaseMeme);
    })

  }, [])


  return (
      <div style={{ textAlign: "center" }}>
        {(
          <>
            {/* {templates.map(template => {
              return (
                <Meme
                  template={template}
                  onClick={() => {
                    setTemplate(template);
                  }}
                />
              );
            })} */}
          </>
        )}
      </div>
  );
};

export default DisplayTemplates;