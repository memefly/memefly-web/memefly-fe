import { Alert, AlertTitle } from "@material-ui/lab";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { generateMeme, uploadImage } from "../../store/actions/actions";
import ImgUpload from "./ImgUpload/ImgUpload.js";
import { SaveImg } from "../ImgEditor/Save-Img/saveImg";
import ImgContainer from "./ImgContainer";

function downloadImg() {
	const GrabCanvas = document.getElementById("d");
}

function GenerateMemePage(props) {
	
	const [meme, setMeme] = useState();
	const handleGenerateMeme = (e) => {
		e.preventDefault();
		setMeme(props.generateMeme());
	};

	useEffect(() => {
		if (props.meme_url === "") {
			props.generateMeme();
		}
	}, [props.meme_url]);

	return (
		<div className="MainContainer">
			<div className="MemeContainer">
				<div style={{ padding: "2rem" }}>
					<Alert severity="warning">
						<AlertTitle>Warning</AlertTitle>
						*THIS DATA WAS TRAINED ON DATA STRAIGHT FROM THE INTERNET, SOME MESSAGES
						MAY NOT BE SAFE FOR THOSE UNDER THE AGE OF 18.
						<br />
						*Content generated does not reflect the views, values, or morals of the
						creators. We tried to keep it clean...The internet is filthy.
					</Alert>
				</div>
				<div id="imageGroup screen">
					<ImgContainer />
				</div>

				<div className="ImageControlWrapper" style={{ padding: "2rem" }}>
					<button
						onClick={handleGenerateMeme}
						className="ButtonDesignOne"
						id="mainGenerateButton"
					>
						GENERATE MEME
					</button>
					<button
						className="ButtonDesignOne"
						id="SaveMemeButton"
						onClick={downloadImg}
					>
						SAVE MEME
					</button>

					<ImgUpload />
					<SaveImg/>
				</div>
				<div
					className="fb-share-button"
					data-href="https://www.memeflyai.com"
					data-layout="button_count"
					data-size="small"
				>
					<a
						target="_blank"
						rel="noopener noreferrer"
						href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.memeflyai.com%2F&amp;src=sdkpreparse"
						className="fb-xfbml-parse-ignore"
					>
						Share
					</a>
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		meme_data: state.memeReducer.meme,
		meme_url: state.memeReducer.meme.meme_url,
	};
};

export default connect(mapStateToProps, { generateMeme, uploadImage })(
	GenerateMemePage
);
