
export function findWidthDifference(containerSize, elementWidth) {
	return containerSize - elementWidth;
}

export function findLeftCoordinate(containerSize, elementWidth) {
	return findWidthDifference(containerSize, elementWidth) / 2;
}

export function scaleImage(imgSrc, dimension){
	return imgSrc + dimension;
}