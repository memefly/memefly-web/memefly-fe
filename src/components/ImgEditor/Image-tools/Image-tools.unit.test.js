import { scaleImage } from "./Image-tools";
import { findLeftCoordinate, findWidthDifference } from "./ImgContainer";

describe("ScalesImage", () => {
	it("Sanity Check", () => {
		expect(scaleImage(2, 2)).toBe(4);
	});
});

describe("Determines the left coordinate of a text box should be", () => {
	it("returns the difference between container and element width", () => {
		expect(findWidthDifference(10, 10)).toBe(0);
		expect(findWidthDifference(10, 5)).toBe(5);
	});

	it("Returns difference of container and element width divided by 2", () => {
		expect(findLeftCoordinate(10, 5)).toBe(2.5);
		expect(findLeftCoordinate(0, 0)).toBe(0);
	});
});
