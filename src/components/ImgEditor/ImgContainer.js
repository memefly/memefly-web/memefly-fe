import { fabric } from "fabric";
import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";

import { findLeftCoordinate } from "./Image-tools/Image-tools";

function ImgContainer({ meme_url, generated_meme_texts }) {
	const [imgSize, setImgSize] = useState({ width: 500, height: 500, left: 10 });

	// DISPLAYS A RANDOM MESSAGE IF THERE IS MORE THAN 1 CHOICE.
	function randomMessage() {
		if (!generated_meme_texts.length) {
			return generated_meme_texts;
		} else {
			return generated_meme_texts[
				Math.floor(Math.random() * generated_meme_texts.length)
			];
		}
	}

	const textWidth = Math.floor(imgSize.width / 1.05);
	const canvasRef = useRef(null);

	// CANVAS USE EFFECT
	useEffect(() => {
		// PROPERTIES FOR TEXT BOX.
		var textBox = new fabric.Textbox(randomMessage(), {
			top: 16,
			left: imgSize.left,
			width: textWidth,
			minWidth: textWidth,

			fontFamily: "Alegreya Sans SC",
			fill: "white",
			stroke: "black",
			fontWeight: 900,
			fontSize: 30,
			textAlign: "center",

			padding: 3,
			cursorColor: "blue",
			borderColor: "limegreen",
			borderScaleFactor: 3,
		});

		// Creates Canvas
		let canvas = new fabric.Canvas("meme_img_display", {
			// preserveObjectStacking: true,
		});

		// This loads the image
		let meme;
		let memeImg = new Image();

		memeImg.crossOrigin = "anonymous";


		memeImg.onload = function() {
			// Properties for Meme Image
			meme = new fabric.Image(memeImg, {
				angle: 0,
				left: 0,
				top: 0,
				selectable: false,
			}).scaleToWidth(500);
			console.log(`meme image info: ${meme}`);

			let scaledWidth = meme.getScaledWidth();
			let scaledHeight = meme.getScaledHeight();

			// This is like z-index, this keeps the image behind the text
			canvas.add(meme).moveTo(meme, 0);

			// Stores Image Dimensions in imgSize as object.
			setImgSize({
				width: scaledWidth,
				height: scaledHeight,
				left: findLeftCoordinate(scaledWidth, textWidth),
			});

			canvas.add(textBox);
			
			// Sets canvas size to size of image.
			canvas.setWidth(scaledWidth);
			canvas.setHeight(scaledHeight);

			console.log(scaledWidth, scaledHeight)
		};
		

		memeImg.src = meme_url;

		// Cleans up canvas after each new meme is generated
		return function clean_up() {
			canvas.dispose();
		};
	}, [meme_url]);

	return (
		<div>
			<canvas ref={canvasRef} id="meme_img_display" className="CanvasC" />
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		meme_url: state.memeReducer.meme.meme_url,
		generated_meme_texts: state.memeReducer.meme.generated_meme_texts,
	};
};

export default connect(mapStateToProps)(ImgContainer);
